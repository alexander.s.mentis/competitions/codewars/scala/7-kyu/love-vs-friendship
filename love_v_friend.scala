object LoveVsFriendship {
  def loveVsFriendship(s: String): Int = {
    s.foldRight(0)((ch, summ) => ch - 'a' + 1 + summ)
  }
}